import { useContext } from "react";
import { WizardContext } from "../App";

const useStepsData = () => {
  const { stepsData } = useContext(WizardContext);

  return Object.values(stepsData);
};

export { useStepsData };
