import React from "react";
import { WizardContext } from "../App";

const withStepData = Component => props => (
  <WizardContext.Consumer>
    {({ stepsData, stepNumber }) => (
      <Component {...props} stepData={stepsData[stepNumber]} />
    )}
  </WizardContext.Consumer>
);

export { withStepData };
