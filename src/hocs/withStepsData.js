import React from "react";
import { WizardContext } from "../App";

const withStepsData = Component => props => (
  <WizardContext.Consumer>
    {({ stepsData }) => (
      <Component {...props} stepsData={Object.values(stepsData)} />
    )}
  </WizardContext.Consumer>
);

export { withStepsData };
