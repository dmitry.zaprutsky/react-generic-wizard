import React, { memo } from "react";
import PropTypes from "prop-types";
import { Button, Icon } from "antd";
import "./wizard-navigation.scss";

const WizardNavigation = ({ onBackClick, onNextClick, onSubmitClick }) => (
  <div className="wizard-navigation">
    {onBackClick && (
      <Button onClick={onBackClick}>
        <Icon type="left" />
        Back
      </Button>
    )}
    {onNextClick && (
      <Button type="primary" onClick={onNextClick}>
        Continue
        <Icon type="right" />
      </Button>
    )}
    {onSubmitClick && (
      <Button type="primary" onClick={onSubmitClick}>
        Submit
      </Button>
    )}
  </div>
);

WizardNavigation.prototype = {
  onPreviousClick: PropTypes.func,
  onNextClick: PropTypes.func,
  onSubmitClick: PropTypes.func
};

export default memo(WizardNavigation);
