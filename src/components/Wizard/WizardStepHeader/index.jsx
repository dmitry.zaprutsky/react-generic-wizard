import React from "react";
import PropTypes from "prop-types";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { ANIMATION_DURATION } from "../config";
import "./wizard-step-header.scss";

const WizardStepHeader = ({ stepName, stepNumber }) => (
  <TransitionGroup className="wizard-step-header-wrapper">
    <CSSTransition
      key={stepName}
      timeout={ANIMATION_DURATION * 2}
      classNames="wizard-step-header"
    >
      <div className="wizard-step-header">
        <h3 className="wizard-step-header-number">{`Step ${stepNumber +
          1}`}</h3>
        <h1 className="wizard-step-header-title">{stepName}</h1>
      </div>
    </CSSTransition>
  </TransitionGroup>
);

WizardStepHeader.propTypes = {
  stepName: PropTypes.string.isRequired,
  stepNumber: PropTypes.number.isRequired
};

export default WizardStepHeader;
