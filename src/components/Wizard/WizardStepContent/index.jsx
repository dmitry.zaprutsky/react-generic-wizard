import React from "react";
import PropTypes from "prop-types";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { ANIMATION_DURATION } from "../config";
import "./wizard-step-content.scss";

const ANIMATION_DELAY = 100;

const WizardStepContent = ({ stepName, stepComponent }) => (
  <TransitionGroup className="wizard-step-content-wrapper">
    <CSSTransition
      key={stepName}
      timeout={ANIMATION_DURATION - ANIMATION_DELAY}
      classNames="wizard-step-content"
    >
      <div>{stepComponent}</div>
    </CSSTransition>
  </TransitionGroup>
);

WizardStepContent.propTypes = {
  stepName: PropTypes.string.isRequired,
  stepComponent: PropTypes.element.isRequired
};

export default WizardStepContent;
