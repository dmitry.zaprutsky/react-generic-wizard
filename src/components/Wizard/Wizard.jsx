import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import WizardProgress from "./WizardProgress";
import WizardNavigation from "./WizardNavigation";
import WizardStepHeader from "./WizardStepHeader";
import WizardStepContent from "./WizardStepContent";
import "./wizard.scss";

const Wizard = ({
  steps,
  stepNumber,
  onNextStep,
  onBackStep,
  onSubmit,
  isGoBack,
  onGoToStep
}) => {
  const currentStep = steps[stepNumber];
  const hasBackButton = stepNumber > 0;
  const hasNextButton = stepNumber < steps.length - 1;

  return (
    <div className="container">
      <WizardProgress
        steps={steps}
        stepNumber={stepNumber}
        onGoToStep={onGoToStep}
      />
      <div
        className={classNames("wizard-step", {
          "wizard-step-back": isGoBack
        })}
      >
        <WizardStepHeader stepName={currentStep.name} stepNumber={stepNumber} />
        <WizardStepContent
          stepName={currentStep.name}
          stepComponent={currentStep.component}
        />
        <WizardNavigation
          onBackClick={hasBackButton && onBackStep}
          onNextClick={hasNextButton && onNextStep}
          onSubmitClick={!hasNextButton && onSubmit}
        />
      </div>
    </div>
  );
};

Wizard.propTypes = {
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
        .isRequired,
      component: PropTypes.element.isRequired
    })
  ).isRequired,
  stepNumber: PropTypes.number.isRequired,
  isGoBack: PropTypes.bool.isRequired,
  onNextStep: PropTypes.func.isRequired,
  onBackStep: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onGoToStep: PropTypes.func.isRequired
};

export default Wizard;
