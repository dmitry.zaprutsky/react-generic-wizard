import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import WizardDoneIcon from "../WizardDoneIcon";
import "./wizard-progress.scss";

const WizardProgress = ({ steps, stepNumber, onGoToStep }) => (
  <div className="wizard-progress">
    {steps.map((step, index) => {
      const isDone = index < stepNumber;
      return (
        <div
          key={index}
          className={classNames("wizard-progress-item", {
            current: index === stepNumber,
            done: isDone
          })}
        >
          <div className="wizard-progress-line" />
          <div
            className="wizard-progress-step"
            onClick={() => onGoToStep(index)}
          >
            <span className="wizard-progress-step-icon">
              {isDone ? (
                <WizardDoneIcon className="wizard-progress-step-icon-done" />
              ) : (
                index + 1
              )}
            </span>
          </div>
          <span className="wizard-progress-label">{step.name}</span>
        </div>
      );
    })}
  </div>
);

WizardProgress.prototype = {
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      component: PropTypes.element.isRequired
    })
  ).isRequired,
  stepNumber: PropTypes.number.isRequired,
  onGoToStep: PropTypes.func.isRequired
};

export default WizardProgress;
