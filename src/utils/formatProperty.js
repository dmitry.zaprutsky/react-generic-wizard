const formatProperty = str =>
  str
    .toLowerCase()
    .split(" ")
    .join("-");

export default formatProperty;
