const sentencize = str => {
  const lower = str
    .toLowerCase()
    .split("-")
    .join(" ");
  return `${lower[0].toUpperCase()}${lower.slice(1)}`;
};

export default sentencize;
