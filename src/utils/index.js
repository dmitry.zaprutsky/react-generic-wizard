import sentencize from "./sentenceCase";
import formatProperty from "./formatProperty";

export { sentencize, formatProperty };
