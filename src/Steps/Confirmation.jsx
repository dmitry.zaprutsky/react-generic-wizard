import React from "react";
import { List, Typography } from "antd";
import { useStepsData } from "../hooks/useStepsData";
import { sentencize } from "../utils";

const Confirmation = () => {
  const stepsData = useStepsData();

  return (
    <div>
      {stepsData.map((stepData, index) => {
        const data = Object.entries(stepData).map(([name, value]) => ({
          name,
          value
        }));
        return (
          <React.Fragment key={index}>
            <List
              size="small"
              bordered
              key={index}
              dataSource={data}
              renderItem={({ name, value }) => {
                return (
                  <List.Item>
                    <Typography.Text strong>{sentencize(name)}</Typography.Text>
                    : <Typography.Text>{value}</Typography.Text>
                  </List.Item>
                );
              }}
            />
            <br />
          </React.Fragment>
        );
      })}
    </div>
  );
};

export default Confirmation;
