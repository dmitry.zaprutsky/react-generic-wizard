import React from "react";
import { Form } from "antd";
import { FormItem, FormRadioGroupItem } from "./components/";
import { formEnhancer } from "./hocs/formEnhancer";

export const FORM_NAME = "Ship To";

const SERVICES = ["USPS", "DHL Express", "Federal Express", "UPS"];

const ShipTo = props => (
  <Form {...props.formLayout}>
    <FormItem label="Company" {...props} />
    <FormItem label="Address" {...props} />
    <FormItem label="City" {...props} />
    <FormItem label="Region" {...props} />
    <FormItem label="Zip Code" {...props} />
    <FormItem label="Telephone Number" {...props} />
    <FormRadioGroupItem label="Service" values={SERVICES} {...props} />
  </Form>
);

const EnhancedForm = formEnhancer(FORM_NAME, ShipTo);

export default EnhancedForm;
