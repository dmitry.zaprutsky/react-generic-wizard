import React from "react";
import { Form } from "antd";
import { withStepData } from "../../hocs/withStepData";

const FORM_LAYOUT = {
  labelCol: { span: 10 },
  wrapperCol: { span: 6 }
};

const formEnhancer = (name, Component) =>
  withStepData(
    Form.create()(
      class extends React.Component {
        handleSubmit = handler => {
          this.props.form.validateFields((err, values) => {
            if (!err) {
              handler(values);
            }
          });
        };

        render() {
          return <Component formLayout={FORM_LAYOUT} {...this.props} />;
        }
      }
    )
  );

export { formEnhancer };
