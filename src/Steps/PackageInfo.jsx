import React from "react";
import { Form } from "antd";
import { FormItem, FormTextAreaItem } from "./components/";
import { formEnhancer } from "./hocs/formEnhancer";

export const FORM_NAME = "Package Info";

const PackageInfo = props => (
  <Form {...props.formLayout}>
    <FormItem label="Number of packages" {...props} />
    <FormItem label="Total Weight" {...props} />
    <FormItem label="Contents" {...props} />
    <FormItem label="Special Instructions" {...props} />
    <FormTextAreaItem label="Other Instructions" {...props} />
  </Form>
);

const EnhancedForm = formEnhancer(FORM_NAME, PackageInfo);

export default EnhancedForm;
