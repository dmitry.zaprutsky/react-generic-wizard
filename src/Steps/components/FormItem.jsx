import React from "react";
import { Form, Input } from "antd";

const FormItem = ({
  label,
  placeholder = "",
  required = true,
  form: { getFieldDecorator },
  stepData
}) => {
  let field = label.toLowerCase();
  const message = required && `Please input ${field}!`;
  field = field.split(" ").join("-");

  return (
    <Form.Item label={label}>
      {getFieldDecorator(field, {
        initialValue: stepData && stepData[field],
        rules: [
          {
            required,
            message
          }
        ]
      })(<Input placeholder={placeholder} />)}
    </Form.Item>
  );
};

export default FormItem;
