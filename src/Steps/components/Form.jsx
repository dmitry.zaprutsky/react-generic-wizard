import React from "react";
import { Form } from "antd";
import { withStepData } from "../../hocs/withStepData";

export const WizardFormContext = React.createContext();

class WizardForm extends React.PureComponent {
  handleSubmit = handler => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        handler(values);
      }
    });
  };

  render() {
    const { stepDate, form } = this.props;
    return (
      <Form>
        <WizardFormContext.Provider value={{ stepDate, form }}>
          {this.props.children}
        </WizardFormContext.Provider>
      </Form>
    );
  }
}

const EnhancedWizardForm = withStepData(Form.create()(WizardForm));

export default EnhancedWizardForm;
