import React from "react";
import { Form, Radio } from "antd";

const FormRadioGroupItem = ({
  label,
  stepData,
  form: { getFieldDecorator },
  values
}) => (
  <Form.Item label={label}>
    {getFieldDecorator(label, {
      initialValue: stepData && stepData[label]
    })(
      <Radio.Group>
        {values.map((value, index) => (
          <Radio value={value} key={index}>
            {value}
          </Radio>
        ))}
      </Radio.Group>
    )}
  </Form.Item>
);

export default FormRadioGroupItem;
