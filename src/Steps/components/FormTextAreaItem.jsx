import React from "react";
import { Form, Input } from "antd";
import { formatProperty } from "../../utils";

const DEFAULT_ROWS_HEIGHT = 4;

const FormTextAreaItem = ({
  label,
  placeholder = "",
  required = true,
  form: { getFieldDecorator },
  stepData
}) => {
  const field = formatProperty(label);

  return (
    <Form.Item label={label}>
      {getFieldDecorator(field, {
        initialValue: stepData && stepData[field]
      })(<Input.TextArea rows={DEFAULT_ROWS_HEIGHT} />)}
    </Form.Item>
  );
};

export default FormTextAreaItem;
