import React from "react";
import { DatePicker, Form } from "antd";

const FormDateItem = ({ label, stepData, form: { getFieldDecorator } }) => {
  const field = label.toLowerCase();
  return (
    <Form.Item label="Date">
      {getFieldDecorator(field, {
        initialValue: stepData && stepData[field],
        rules: [
          { type: "object", required: true, message: "Please select time!" }
        ]
      })(<DatePicker />)}
    </Form.Item>
  );
};

export default FormDateItem;
