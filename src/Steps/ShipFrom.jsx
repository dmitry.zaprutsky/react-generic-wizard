import React from "react";
import { Form } from "antd";
import { FormItem } from "./components/";
import { formEnhancer } from "./hocs/formEnhancer";

const ShipFromForm = props => (
  <Form {...props.formLayout}>
    <FormItem label="Sender" {...props} />
    <FormItem label="Project Manager" {...props} />
    <FormItem label="Contact Person" {...props} />
    <FormItem label="Contact Phone" {...props} />
    <FormItem label="Department Name" {...props} />
    <FormItem label="Department Mail Stop" {...props} />
  </Form>
);

export const FORM_NAME = "Ship From";

const EnhancedForm = formEnhancer(FORM_NAME, ShipFromForm);

export default EnhancedForm;
