import React, { Component } from "react";
import "reset-css";
import "antd/dist/antd.css";
import { Modal } from "antd";
import Wizard from "./components/Wizard";
import ShipFromForm, {
  FORM_NAME as SHIP_FROM_FORM_NAME
} from "./Steps/ShipFrom";
import ShipToForm, { FORM_NAME as SHIP_TO_FORM_NAME } from "./Steps/ShipTo";
import PackageInfoForm, {
  FORM_NAME as PACKAGE_INFO_FORM_NAME
} from "./Steps/PackageInfo";
import Confirmation from "./Steps/Confirmation";

let currentStepForm = undefined;

const formRef = form => {
  if (form) {
    currentStepForm = form;
  }
};

const commonProps = {
  wrappedComponentRef: formRef
};

const steps = [
  {
    name: SHIP_FROM_FORM_NAME,
    component: <ShipFromForm {...commonProps} />
  },
  {
    name: SHIP_TO_FORM_NAME,
    component: <ShipToForm {...commonProps} />
  },
  {
    name: PACKAGE_INFO_FORM_NAME,
    component: <PackageInfoForm {...commonProps} />
  },
  {
    name: "Confirmation",
    component: <Confirmation />
  }
];

export const WizardContext = React.createContext();

const INIT_STATE = {
  stepsData: {},
  stepNumber: 0,
  isGoBack: false
};

class App extends Component {
  state = INIT_STATE;

  handleNextStep = () => {
    currentStepForm.handleSubmit(stepValues => {
      this.setState(({ stepNumber, stepsData }) => ({
        stepNumber: stepNumber + 1,
        isGoBack: false,
        stepsData: {
          ...stepsData,
          [stepNumber]: stepValues
        }
      }));
    });
  };

  handleBackStep = () => {
    this.setState(({ stepNumber }) => ({
      stepNumber: stepNumber - 1,
      isGoBack: true
    }));
  };

  handleGoToStep = newStepNumber => {
    const { stepNumber } = this.state;

    if (newStepNumber < stepNumber) {
      this.setState({
        stepNumber: newStepNumber,
        isGoBack: true
      });
    }
  };

  handleSubmit = () => {
    Modal.confirm({
      title: "Do you Want to submit these items?",
      onOk: () => {
        this.setState(INIT_STATE);
      }
    });
  };

  render() {
    const { stepsData, stepNumber, isGoBack } = this.state;

    return (
      <div className="App">
        <WizardContext.Provider
          value={{
            stepsData: stepsData,
            stepNumber: stepNumber
          }}
        >
          <Wizard
            steps={steps}
            stepNumber={stepNumber}
            isGoBack={isGoBack}
            onGoToStep={this.handleGoToStep}
            onNextStep={this.handleNextStep}
            onBackStep={this.handleBackStep}
            onSubmit={this.handleSubmit}
          />
        </WizardContext.Provider>
      </div>
    );
  }
}

export default App;
